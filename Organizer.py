__author__ = 'john'

import os, pwd, grp
from media_auto_organizer import organizer
from media_auto_organizer.settings import Settings
import argparse



def initialize_parser(parser):
    parser.add_argument('--config', help='Config file path')
    parser.add_argument('--rescan_subs', help='scan for missing subtitles', action='store_true')


parser = argparse.ArgumentParser()
initialize_parser(parser)
args = parser.parse_args()


if (args.config):
    conf_path = args.config
else:
    conf_path = os.path.abspath(os.path.join('media_auto_organizer', 'conf'))

Settings.load_config(conf_path)


#----------setup user and group
if Settings.user :
    uid = pwd.getpwnam(Settings.user).pw_uid
    try:
        os.setuid(uid)
    except PermissionError as e:
        print("Cannot execute as %s :%s" % (Settings.user, str(e)))
if Settings.group :
    gid= grp.getgrnam(Settings.group).gr_gid
    try:
        os.setgid(gid)
    except PermissionError as e:
        print("Cannot execute as %s :%s" % (Settings.group, str(e)))


org = organizer.Organizer(conf_path)
#initial scan
org.run()

#rescan for subtitles
if args.rescan_subs:
    org.rescan_for_subtitles()










