__author__ = 'john'

import os
import hashlib
import logging
from persistance import db_schema
from identifiers import opensubtitles
from metadata_finders import imdb

logging = logging.getLogger(__name__)


class MediaScanner():

    def __init__(self, db, object_factory):
        self.db = db
        self.factory = object_factory
        self.identifier = opensubtitles.Opensubtitles(object_factory)
        self.metadata = imdb.Imdb(object_factory)

    def scan_all_files_under_folder(self, path, min_size=0):
        """
        Finds for files under a path.
        minSize :minimum size of file
        """
        found_files = list()
        logging.info("Starting filescanning at %s"% path)
        for root, subFolders, files in os.walk(path):
            if files:
                for file in files:
                    full_path = os.path.join(root, file)
                    try:
                        rec = self.factory.file(full_path)
                        if rec.size >= min_size:
                            found_files.append(rec)
                    except Exception as e:
                        logging.error("Could not open file "+str(e))
                        continue
        logging.info("Found %d files" % len(files))
        return found_files

    def filter_old(self, files):
        """
        Removes files that already exists in database
        """
        return [f for f in files if len(self.db.find_files(path=f.path)) == 0]

    def filter_video(self, files):
        """
        Removes all files that are not video
        """
        return [f for f in files if self.identifier.get_filetype(f.path)=='video']

    def identify_files(self, *files):
        #recognize
        logging.info("Starting media recognition")
        recognized = self.identifier.identify_files(*files)
        logging.info("Recongnized %d media" % len(recognized))
        return recognized

    def get_metadata_and_save(self, *files):
        """
        Analyze and store files discovered files
        1: metadata, Get more metadata from various sources
        2: store in db
        """
        inserted = list()
        for file in files:
            media = file.media
            logging.info("Inserting file : %s" % file.path)
            #metadata
            if not self.db.exists(type(media), imdbid=media.imdbid):
                if type(media) is db_schema.Movie:
                    logging.info('Movie is new, getting metadata')
                    try:
                        self.metadata.update_metadata(file)
                    except LookupError:
                        logging.error("Media recognized but could not get metadata : %s" % file.path)
                        continue
                elif type(media) is db_schema.Episode:
                    logging.info('Episode is new, getting metadata')
                    try:
                        self.metadata.update_metadata(file)
                    except LookupError:
                        logging.error("Media recognized but could not get metadata : %s" % file.path)
                        continue
            else:
                logging.info('Media exists')
                del file.media
                media = self.db.query(type(media), imdbid=media.imdbid)
                file.media = media

            #store in db
            self.db.save_file(file)

            inserted.append(file)

        return inserted









