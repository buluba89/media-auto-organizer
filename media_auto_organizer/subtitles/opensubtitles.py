__author__ = 'john'

import scrappers.opensubtitles
import difflib
import logging
import os



class Opensubtitles():

    def __init__(self, object_factory):
        self.factory = object_factory

    def get_subtitle(self, file, lang):
        with scrappers.opensubtitles.OpenSubtitles() as op:
            tmp = self.factory.tmp_file()
            try:
                subtitles = op.get_subtitles(file.path, lang)
                #if not found return None
                if not subtitles:
                    return None
                best_sub = max(subtitles, key= lambda x:compare_filenames(file.path, x['filename']))
                #if similarity too small,dont download
                if compare_filenames(file.path, best_sub['filename']) < 0.3:
                    return None
                op.download_subtitle(best_sub['id'], tmp.path)
            except Exception as e:
                logging.error(str(e))
                os.remove(tmp.path)
                raise e
            logging.info("Subtitle for %s downloaded to %s" % (file.path,tmp.path))
            return tmp

def compare_filenames(fn1, fn2):
    #get just filenames without path and extension
    fn1 = os.path.splitext(os.path.basename(fn1))[0].lower()
    fn2 = os.path.splitext(os.path.basename(fn2))[0].lower()
    dif = difflib.SequenceMatcher(None, fn1, fn2).ratio()
    return dif

