__author__ = 'john'

import os
from views import storage
from settings import Settings
from displays import filesystem_display
from persistance import db_schema
import logging


class Storage():

    def __init__(self, db):
        self.db = db
        #series folder
        if not os.path.isdir(Settings.series_storage):
            os.mkdir(Settings.series_storage)
        self.series_folder = filesystem_display.FilesystemDisplay(Settings.series_storage)
        #movies folder
        if not os.path.isdir(Settings.movies_storage):
            os.mkdir(Settings.movies_storage)
        self.movies_folder = filesystem_display.FilesystemDisplay(Settings.movies_storage)

        self.series_view = storage.SeriesStorage()
        self.movies_view = storage.MoviesStorage()

        self.check_consistency()
        self.update_view()

    def update_view(self):
        self.series_view = storage.SeriesStorage()
        self.movies_view = storage.MoviesStorage()

        files = self.db.find_files()
        self.insert_file(*files)

        self.series_folder.set_owner(group=Settings.group)
        self.movies_folder.set_owner(group=Settings.group)

    def insert_file(self, *files):
        for file in files:
            filetype = type(file.media)
            if(filetype is db_schema.Movie):
                self.movies_view.insert_file(file)
            elif (filetype is db_schema.Episode):
                self.series_view.insert_file(file)

        self.movies_folder.apply_view(self.movies_view)
        self.series_folder.apply_view(self.series_view)


    def check_consistency(self):
        files = self.db.find_files()
        for file in files:
            if not os.path.isfile(file.path):
                logging.warning("File %s not found \n deleting from database" % file.path)
                self.db.delete_file(file)
                self.db.commit()
                continue

            #check if file size is the same
            if os.stat(file.path).st_size != file.size:
                file.update_filesize()
                file.update_md5()
                self.db.commit()

