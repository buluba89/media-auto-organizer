__author__ = 'john'

from views.common_objects import *
from persistance import db_schema
import os, grp, pwd
import logging
from send2trash import send2trash

class FilesystemDisplay():
    """Materialize a display to the filesystem so the user can see the whole collection from a filesystem index"""

    def __init__(self, path):

        if not os.path.exists(path):
            os.makedirs(path)

        self.path = path

    def apply_view(self, view):
        """apply the view to filesystem"""
        self.create_tree(self.path, view.root)
        self.clean_tree(self.path, view.root)

    def create_tree(self, path, parent):
        """creates all nodes to the tree in filesystem"""
        for key, value in parent.items():
            new_path = os.path.join(path, key)
            self.make_dir(new_path)
            if type(value) is Folder:
                self.create_tree(new_path, value)
            else:
                try:
                    self.create_record(new_path, value)
                except FileExistsError:
                    continue

    def clean_tree(self, path, parent, depth=0):
        """Checks the filesystem for records tha doesnt exists anymore"""
        folders = os.listdir(path)

        #dont delete anything in root folder
        if depth > 0:
            for folder in folders :
                if folder not in parent.keys():
                    try:
                        send2trash(os.path.join(path, folder))
                    except Exception as e:
                        logging.error("Could not delete folder: %s" % str(e))

        for key,value in parent.items() :
            new_path = os.path.join(path, key)
            self.make_dir(new_path)
            if type(value) is  Folder:
                self.clean_tree(new_path, value, depth+1)

    def make_dir(self, path):
        if not os.path.exists(path):
            os.makedirs(path)

    def create_record(self, path, contents):
        """get leafs of the tree and creates them. It can be links or files etc"""
        for content in contents:
            final_path = os.path.join(path, os.path.basename(content.path))
            if type(content) is Link:
                if os.path.islink(path):
                    os.remove(path)
                os.symlink(content.path, final_path)
            if type(content) is db_schema.File:
                if os.path.split(content.path) != final_path:
                    content.move_file(final_path)

    def set_permissions(self,octal, path=None):
        if not path:
            path = self.path

        for root, dirs, files in os.walk(path):
            for momo in dirs:
                os.chmod(os.path.join(root, momo), octal)

            for momo in files:
                os.chmod(os.path.join(root, momo), octal)

    def set_owner(self, username=None, group=None, path=None):

        if not path:
            path = self.path

        uid = gid = -1
        if username:
            uid = pwd.getpwnam(username).pw_uid
        if group:
            gid= grp.getgrnam(group).gr_gid

        for root, dirs, files in os.walk(path):
            for momo in dirs:
                try:
                    os.chown(os.path.join(root, momo), uid, gid)
                except PermissionError:
                    continue

            for momo in files:
                try:
                    os.chown(os.path.join(root, momo), uid, gid)
                except PermissionError:
                    continue






