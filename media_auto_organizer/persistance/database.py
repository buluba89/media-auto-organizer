__author__ = 'john'
import logging

from persistance.db_schema import *
from sqlalchemy import create_engine, event, inspect
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import ClauseElement
from sqlalchemy.exc import IntegrityError, OperationalError
import os
import time
from sqlalchemy import event


def _fk_pragma_on_connect(dbapi_con, con_record):
    dbapi_con.execute('pragma foreign_keys=ON')

class Database():
    """
    The database object is responsible for making all interactions with the database
    """
    def __init__(self, path):
        self.engine = create_engine('sqlite:///'+ os.path.join(path, 'organizer.db'), echo=False)
        #sqlite doesnt support foreignkeys by default, must be enabled on every connection
        event.listen(self.engine, 'connect', _fk_pragma_on_connect)
        #Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        #self.session = Session(autocommit=False)
        self.session = Session(autoflush=True)

    def find_files(self,**kwargs):
        """Find files that matches to the arguments"""
        return  self.session.query(File).filter_by(**kwargs).all()

    def get_media(self):
        return  self.session.query(File).filter(File.media_id >= 1).all()

    def save_file(self, file):
        """ Insert file into session and commits to the db"""
        try:
            file.update_md5()
            file.update_filesize()
            self.session.add(file)
            self.commit()
        except IntegrityError as e:
            self.session.rollback()
            logging.error("Error inserting file %s :%s" % (file.path, str(e)))

    def delete_file(self, file):
        self.session.delete(file)
        self.commit()

    def insert_obj(self,type, **kwargs):
        """Get or create various object from database"""
        obj,_ = self.get_or_create(type, **kwargs)
        self.session.add(obj)
        self.commit()
        return obj

    def commit(self):
        """makes the actual commit to the database"""
        for i in range(5):
            try:
                self.session.commit()
                return
            except OperationalError:
                logging.error('Database is locked, waiting 3 seconds')
                time.sleep(3)

    def exists(self, model, **kwargs):
        if self.query(model, **kwargs):
            return True
        return False

    def query(self, model, **kwargs):
        """query the database """
        return self.session.query(model).filter_by(**kwargs).first()

    def get_or_create(self, model, defaults=None, **kwargs):
        """ if a db record exists returns the corresponding object, if it doesnt creates it"""
        instance = self.session.query(model).filter_by(**kwargs).first()
        if instance:
            return instance, False
        else:
            params = dict((k, v) for k, v in kwargs.items() if not isinstance(v, ClauseElement))
            params.update(defaults or {})
            instance = model(**params)
            self.session.add(instance)
            self.session.commit()
            return instance, True


if __name__ == "__main__":
    pass


