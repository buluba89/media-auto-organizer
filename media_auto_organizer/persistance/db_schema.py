__author__ = 'john'

"""
This module contains the schema classes tha sqlalchemy needs to create and interact with the database
"""

from sqlalchemy import Column, Integer, BigInteger, String, Float, Date, DateTime, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from datetime import datetime
import shutil
import hashlib
import logging
import os, pwd, grp

from settings import  Settings

Base = declarative_base()


class RelatedFile(Base):
    __tablename__ = 'related_files'
    file_id = Column(Integer, ForeignKey('files.id'), primary_key=True)
    for_file_id = Column(Integer, ForeignKey('files.id'))
    type = Column(Integer)

    file = relationship("File", foreign_keys=[file_id], single_parent=True,
                        cascade="save-update, merge, delete, delete-orphan")

    __mapper_args__ = {'polymorphic_on': type}


class File(Base):
    __tablename__ = 'files'

    id = Column(Integer, primary_key=True)
    media_id = Column(Integer, ForeignKey('media.id'))
    path = Column(String, unique=True)
    size = Column(Integer)
    md5 = Column(String)
    date_imported = Column(DateTime, default=datetime.now)
    media = relationship('Media', backref="files")
    related_files = relationship("RelatedFile", foreign_keys=[RelatedFile.for_file_id], backref='for_file',
                             cascade="save-update, merge, delete")
    related_for = relationship("RelatedFile", foreign_keys=[RelatedFile.file_id], single_parent=True,
                               cascade="save-update, merge, delete")

    def load_from_db(self, db):
        obj = db.query(self.db_map, path=self.path)
        media = Media.create_from_db(db, obj.media)
        related_files = list()
        for sub in obj.related_files:
            subtitle = Subtitle.create_from_db(db, sub)
            related_files.append(subtitle)

        self.__init__(obj.path, obj.size, media, related_files, obj.md5)

        self.db_object = obj

    def add_related_file(self, related_file):
        self.related_files.append(related_file)

    def move_file(self, new_path):
        """
        Moves the file to a new path
        """
        uid, gid = os.stat(self.path).st_uid, os.stat(self.path).st_gid
        logging.info("Moving file %s to %s" % (self.path, new_path))
        shutil.move(self.path, new_path)
        self.path = new_path
        #self.set_owner(uid, gid)
        self._sa_instance_state.session.commit()

    def rename_file(self, name):
        uid, gid = os.stat(self.path).st_uid, os.stat(self.path).st_gid
        logging.info("Renameing file %s to %s" % (self.path, name))
        new_path = os.path.join(os.path.dirname(self.path), name)
        os.rename(self.path, new_path )
        self.path = new_path
        #self.set_owner(uid, gid)
        self._sa_instance_state.session.commit()

    def get_filename(self):
        base = os.path.basename(self.path)
        return os.path.splitext(base)[0]

    def update_md5(self, block_size=256*128):
        '''
        Block size directly depends on the block size of your filesystem
        to avoid performances issues
        Here I have blocks of 4096 octets (Default NTFS)
        '''
        logging.info("Computing md5 for %s" % self.path)
        try:
            if os.name is 'posix':
                dirpath = os.path.dirname(self.path)
                stat = os.statvfs(dirpath)
                block_size = stat.f_bsize
        except:
            pass
        md5 = hashlib.md5()
        with open(self.path, 'rb') as f:
            for chunk in iter(lambda: f.read(block_size), b''):
                md5.update(chunk)

        self.md5 = md5.hexdigest()

    def update_filesize(self):
        self.size=os.stat(self.path).st_size

    def set_owner(self, uid=-1, gid=-1):

        if type(uid) is str:
            uid = pwd.getpwnam(uid).pw_uid
        if type(gid) is str:
            gid= grp.getgrnam(gid).gr_gid
        try:
            os.chown(self.path, uid, gid)
        except FileNotFoundError:
            pass

    def set_permissions(self, octal):
        try:
            os.chmod(self.path, octal)
        except FileNotFoundError:
            pass

    def __repr__(self):
        return "<File(path='%s')>" % self.path

genre_association_table = Table('genre_m2m', Base.metadata,
    Column('media_id', Integer, ForeignKey('media.id')),
    Column('genre_id', Integer, ForeignKey('genres.id'))
)

class Media(Base):
    __tablename__ = 'media'

    id = Column(Integer, primary_key=True)
    type = Column("type", Integer)
    genres = relationship("Genre", secondary=genre_association_table, backref="media")
    persons = relationship("PersonM2M", backref="media")

    __mapper_args__ = {'polymorphic_on': type}


class Movie(Media):
    __tablename__ = 'movies'
    __mapper_args__ = {'polymorphic_identity': 1}
    movie_id = Column('id',Integer,ForeignKey('media.id'), primary_key=True)
    imdbid = Column(String, unique=True, primary_key=True)
    tagline = Column(String)
    title = Column(String)
    rating = Column(Float)
    year = Column(Integer)
    runtime = Column(String)
    release_date = Column(Date)
    plot_outline = Column(String)
    cover_url = Column(String)
    poster_url = Column(String)

    def __repr__(self):
        return "<Movie(title='%s')>" % self.title

class Series(Base):
    __tablename__ = 'series'
    id = Column(Integer, primary_key=True)
    imdbid = Column(String, unique=True)
    tagline = Column(String)
    title = Column(String)
    rating = Column(Float)
    year = Column(Integer)
    plot_outline = Column(String)
    cover_url = Column(String)
    poster_url = Column(String)
    episodes = relationship("Episode",backref="series")


class Episode(Media):
    __tablename__ = 'episodes'
    __mapper_args__ = {'polymorphic_identity': 2}
    episode_id = Column('id',Integer,ForeignKey('media.id'), primary_key=True)
    imdbid = Column(String, unique=True,primary_key=True)
    series_id = Column(Integer,ForeignKey('series.id'))
    season = Column(Integer)
    episode_no = Column(Integer)
    tagline = Column(String)
    title = Column(String)
    rating = Column(Float)
    year = Column(Integer)
    runtime = Column(String)
    release_date = Column(Date)
    plot_outline = Column(String)
    cover_url = Column(String)
    poster_url = Column(String)

    def __repr__(self):
        return "<Episode(series='%s', title='%s', season='%d', episode='%d')>" % (
                self.series, self.title, self.season, self.episode_no)
class Genre(Base):
    __tablename__= 'genres'
    id = Column(Integer, primary_key=True)
    name = Column(String)


class Person(Base):
    __tablename__ = 'persons'
    id = Column(Integer,primary_key=True)
    imdbid = Column(String, unique=True)
    name = Column(String)


class PersonM2M(Base):
    __tablename__='person_m2m'
    id = Column(Integer,primary_key=True)
    media_id = Column(Integer, ForeignKey('media.id'))
    person_id = Column(Integer, ForeignKey('persons.id'))
    job_id = Column(Integer,ForeignKey('jobs.id'))
    job = relationship('Job')
    person = relationship("Person", backref="media_assocs")


class Job(Base):
    __tablename__ = "jobs"
    id = Column(Integer,primary_key=True)
    description = Column(String)


class Subtitle(RelatedFile):
    __tablename__ = 'subtitles'
    __mapper_args__ = {'polymorphic_identity': 1}
    file_id = Column(Integer, ForeignKey('related_files.file_id'),  primary_key=True)
    language = Column(String)
