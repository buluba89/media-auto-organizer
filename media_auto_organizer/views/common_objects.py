__author__ = 'john'

from collections import defaultdict

class Link():
    """
    A link representation for use in views
    """
    def __init__(self, path=None):
        self.path = path

    def __repr__(self):
        return "<Link(path='%s')>" % self.path


class Folder(defaultdict):
    """
    A folder tree represantion using default dicts
    """
    def __init__(self, *args, **kwargs):
        super(Folder, self).__init__(Folder)

    def __repr__(self):
        return "<Folder>"