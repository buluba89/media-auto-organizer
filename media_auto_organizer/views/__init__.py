__author__ = 'john'

from abc import ABC, abstractmethod
from persistance import db_schema

class View(ABC):

    def create_episode_folder(self, episode):
        #Episode folder, common for all categories
        episode_folder = list()
        episode_folder.append(episode)
        #add subtitles
        self.rename_subtitles(episode)
        for rel in episode.related_files:
            episode_folder.append(rel.file)

        return episode_folder

    def create_movie_folder(self, movie):
        #Movie folder, common for all categories
        movie_folder = list()
        movie_folder.append(movie)
        #add subtitles
        self.rename_subtitles(movie)
        for rel in movie.related_files:
            movie_folder.append(rel.file)

        return movie_folder

    def add_content_folder(self, folder, subfolder_name, contentlist):

        if type(folder[subfolder_name]) is list:
            folder[subfolder_name].extend(contentlist)
        else:
            folder[subfolder_name] = contentlist


    def rename_subtitles(self, file):
        videoname = file.get_filename()
        subs = [file for file in file.related_files if type(file) is db_schema.Subtitle]
        for sub in subs:
            #rename the subtitle to be like video filename

            sub.file.rename_file("%s-%s.srt" % (videoname, sub.language))