__author__ = 'john'

from abc import abstractmethod
import views
from .common_objects import *
from persistance import db_schema

class StorageView(views.View):
    """
    A view for storage. simplest way to organize media. Transfers everything to prespecified folder
    """
    type = None

    def __init__(self):
        self.root = Folder()

    def insert_file(self, file):

        if type(file.media) is self.type:
            self._insert_file(file)

    @abstractmethod
    def _insert_file(self, file):
        pass


class SeriesStorage(StorageView):

    type = db_schema.Episode

    def _insert_file(self, file):

        media = file.media

        episode = self.create_episode_folder(file)

        #---Structure----
        #Create a folder for series
        series_folder = self.root[media.series.title]
        season = series_folder['Season '+ str(media.season)]

        #add folder contents
        self.add_content_folder(season, str(media.episode_no), episode)


class MoviesStorage(StorageView):

    type = db_schema.Movie

    def __init__(self):
        self.root = Folder()

    def _insert_file(self, file):

        media = file.media
        #Movie, common for all categories
        movie = self.create_movie_folder(file)

        #add movie to folder
        self.add_content_folder(self.root, media.title, movie)