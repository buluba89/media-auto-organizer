import math
import logging
import views
from persistance import db_schema
from  views.common_objects import *

#TODO: add recently added folder

class CategoriesTree(views.View):
    """
    An almost complete view, using all the info available in database.
    It creates a tree tha represent the whole database
    """

    def __init__(self):
        self.root = Folder()

    #TODO: If failed rescan media info
    def insert_files(self, *files):
        for file in files:
            try:
                if type(file.media) is db_schema.Episode:
                    self.insert_episode(file)
                elif type(file.media) is db_schema.Movie:
                    self.insert_movie(file)
            except Exception as e:
                logging.error("Could not insert file in view %s : %s" %(str(file),str(e)))


    def insert_episode(self, file):

        media = file.media

        #Episode, common for all categories
        episode = self.create_episode_folder(file)

        #Create Series folder
        series = self.root["Series"]

        #---------Alphabetically
        alpha = series["Alphabetically"]
        letter = alpha[media.series.title[0]]
        series_folder = letter[media.series.title]
        season = series_folder['Season '+ str(media.season)]
        season[str(media.episode_no)] = episode

        #--------By Series Year
        year = series["Series Year"]
        selected_year = year[str(media.series.year)]
        selected_year[media.series.title]= series_folder

        #------Episode Year
        eyear = series["Episode Year"]
        selected_eyear = eyear[str(media.year)]
        eyear_series_folder = selected_eyear[media.series.title]
        eyear_season=eyear_series_folder['Season '+ str(media.season)]
        eyear_season[str(media.episode_no)]= episode

        #--------By Genre
        genre_folder = series["Genre"]
        for genre in media.genres:
            g = genre_folder[genre.name]
            g[media.series.title] = series_folder

        #-------By Rating
        byrating = series["Rating"]
        rating_folder = byrating[str(math.floor(media.series.rating))]
        rating_folder[media.series.title] = series_folder

        #------All
        all_folder = series["All"]
        all_folder[media.series.title] = series_folder

    def insert_movie(self,file):

        media = file.media

        #Episode, common for all categories
        movie= self.create_movie_folder(file)

        #Create Movies folder
        movies = self.root["Movies"]

        #---------Alphabetically
        alpha = movies["Alphabetically"]
        letter = alpha[media.title[0]]
        letter[media.title] = movie

        #--------By Movie Year
        year = movies["Year"]
        selected_year = year[str(media.year)]
        selected_year[media.title]= movie

        #--------By Genre
        genre_folder = movies["Genre"]
        for genre in media.genres:
            g = genre_folder[genre.name]
            g[media.title] = movie

        #-------By Rating
        byrating = movies["Rating"]
        rating_folder = byrating[str(math.floor(media.rating))]
        rating_folder[media.title] = movie

        #--------All
        all_movies = movies["All"]
        all_movies[media.title] = movie

    def create_episode_folder(self, episode):
        folder = super().create_movie_folder(episode)
        return [Link(file.path) for file in folder]

    def create_movie_folder(self, movie):
        folder = super().create_movie_folder(movie)
        return [Link(file.path) for file in folder]
