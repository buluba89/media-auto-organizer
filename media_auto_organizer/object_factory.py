__author__ = 'john'

from persistance import db_schema
from settings import Settings
import os, grp
import tempfile

class ObjectFactory():

    def __init__(self, db):
        self.db = db

    def file(self, path):
        file = db_schema.File(path=path)
        file.update_filesize()
        return file

    def tmp_file(self):
        subfile, subfilepath = tempfile.mkstemp()
        os.close(subfile)
        return self.file(path=subfilepath)

    def episode(self, imdbid=None, series=None, season=None, episode_no=None, tagline=None, title=None, rating=None,
                year=None, runtime=None, release_date=None, plot_outline=None, cover_url=None, poster_url=None):


        episode = db_schema.Episode(imdbid=imdbid, series=series, season=season,
                                    episode_no=episode_no, tagline=tagline,title=title, rating=rating,
                                    year=year, runtime=runtime, release_date=release_date,
                                    plot_outline=plot_outline, cover_url=cover_url, poster_url=poster_url)
        return episode

    def movie(self, imdbid=None, tagline=None, title=None, rating=None,year=None, runtime=None, release_date=None,
              plot_outline=None, cover_url=None, poster_url=None):

        movie = db_schema.Movie(imdbid=imdbid, tagline=tagline,title=title,
                                rating=rating, year=year, runtime=runtime, release_date=release_date,
                                plot_outline=plot_outline, cover_url=cover_url, poster_url=poster_url)
        return movie

    def series(self, imdbid=None, tagline=None, title=None, rating=None,year=None, plot_outline=None,
               cover_url=None, poster_url=None):

        series,_ = self.db.get_or_create(db_schema.Series, imdbid=imdbid, tagline=tagline,title=title, rating=rating, year=year,
                                  plot_outline=plot_outline, cover_url=cover_url, poster_url=poster_url)

        return series

    def genre(self, name):
        genre, _ = self.db.get_or_create(db_schema.Genre, name=name)
        return genre

    def person(self,imdbid=None, name=None, job=None, media=None):
        person, _ = self.db.get_or_create(db_schema.Person, imdbid=imdbid, name=name)

        if job and media:
            self.add_job_for_person(media, person, job)

        return person

    def job(self, description):
        job, _ = self.db.get_or_create(db_schema.Job, description=description)
        return job

    def add_job_for_person(sel, media, person, job):
        asc = db_schema.PersonM2M()
        asc.job = job
        asc.person = person
        media.persons.append(asc)

    def subtitle(self, file, for_file, language):
        subtitle, _ = self.db.get_or_create(db_schema.Subtitle, file=file, for_file=for_file, language=language)

        return subtitle
