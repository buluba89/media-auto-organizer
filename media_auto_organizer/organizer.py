__author__ = 'john'

import media_scanner
import os
import logging
from settings import Settings
from views.categories import CategoriesTree
from displays.filesystem_display import FilesystemDisplay
from persistance import database, db_schema
from object_factory import ObjectFactory
import subtitles.opensubtitles
import storage


#TODO: add to data types the minimum metadata tha must be, create metadata_geter object

class Organizer ():
    """Represent the whole organizer application. It perform the most high level actions"""

    def __init__(self, config):
        Settings.load_config(config)
        self.db = database.Database(Settings.db_path)
        self.factory = ObjectFactory(self.db)
        self.storage = storage.Storage(self.db)
        logging.info("-----------------------Organizer init---------------------------------")

    def run(self):
        inserted = self.scan_new_media()
        self.download_subtitles(*inserted)
        self.move_to_storage(*inserted)
        #must be after move to storage for the links to be correct
        self.create_filesystem_view()

    #TODO : bring  metada get call here, dont send all files to get metadata, send them one-one
    def scan_new_media(self):
        """scan paths that are defined in settings file"""
        scanner = media_scanner.MediaScanner(self.db, self.factory)

        for path in Settings.media_paths:
            if os.path.isdir(path):
                files = scanner.scan_all_files_under_folder(path,1*8*1024*1024)
            else:
                logging.error("Not valid media path : %s" % path)

        files = scanner.filter_video(files)
        files = scanner.filter_old(files)

        identified = scanner.identify_files(*files)
        return scanner.get_metadata_and_save(*identified)

    def move_to_storage(self, *files):

        self.storage.insert_file(*files)
        self.storage.update_view()
        self.commit()

    def create_filesystem_view(self):
        """create filesystem view"""
        path = Settings.index_path
        view = CategoriesTree()
        files = self.db.find_files()
        view.insert_files(*files)
        dis = FilesystemDisplay(path)
        dis.apply_view(view)

    def download_subtitles(self, *files):
        for file in files:
            subsgeter = subtitles.opensubtitles.Opensubtitles(self.factory)
            subfile = subsgeter.get_subtitle(file, 'ell')
            #if we found a subtitle
            if subfile:
                #append to video
                subtitle = self.factory.subtitle(subfile, file, 'ell')
                file.add_related_file(subtitle)
                #fix permissions
                subfile.set_owner(gid = Settings.group)
                subfile.set_permissions(Settings.permissions)
                self.db.commit()

    def rescan_for_subtitles(self):
        media = self.db.get_media()
        for file in media:
            for related in file.related_files:
                if type(related) is db_schema.Subtitle:
                    break
            else:
                self.download_subtitles(file)


if __name__ == "__main__":
    log = logging.getLogger('media_scanner')
    log.setLevel(logging.INFO)
    logging.basicConfig(level=logging.INFO)
    org = Organizer("conf")
    org.run()




