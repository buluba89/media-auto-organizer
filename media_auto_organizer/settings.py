__author__ = 'john'


class Settings():

    config_path = None
    media_paths = None
    movies_storage = None
    series_storage = None
    index_path = None
    log_path = None
    db_path = None
    user = None
    group = None
    permissions = None

    @staticmethod
    def load_config(path):
        Settings.config_path = path
        config = dict()
        exec(open(path).read(), config)
        Settings.media_paths = config['media_paths']
        Settings.movies_storage = config['movies_storage']
        Settings.series_storage = config['series_storage']
        Settings.index_path = config['index_path']
        Settings.db_path = config['db_path']
        Settings.log_path = config['log_path']
        Settings.user = config['user']
        Settings.group = config['group']
        Settings.permissions = config['permissions']
