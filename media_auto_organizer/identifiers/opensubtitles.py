__author__ = 'john'


import logging
from .identifier import Identifier
from scrappers import opensubtitles


class Opensubtitles(Identifier):
    """Get's data from opensubtitles """

    def __init__(self, object_factory):
        self.factory = object_factory

    def identify_files(self, *files):
        #break in smaller groups because opensubtitles has a limit
        groupslist = [files[i:i+50] for i in range(0, len(files), 50)]

        recognized_list = list()

        for group in groupslist:
            new = self.identify_video(*group)
            recognized_list.extend(new)

        return recognized_list

    def identify_video(self, *files):
            """ Identifies a banch of videos and returns the identified ones"""

            with opensubtitles.OpenSubtitles() as op:
                osinfo = op.get_video_info(*[x.path for x in files])

            recognized = list()

            for f in files:
                if f.path in osinfo:
                    try:
                        f.media = self.extract_opensubtitles_data(osinfo[f.path])
                        recognized.append(f)
                    except TypeError as e:
                        logging.error("Error extracting data from opensubtitles" + str(e))
                        continue

            return recognized

    def extract_opensubtitles_data(self, data):
        """extracts data from scrappers returns to the projects local datatypes"""
        if data["MovieKind"] == "episode" :
            return self.factory.episode(imdbid="tt"+data["MovieImdbID"],
                                         season=int(data["SeriesSeason"]),
                                         episode_no=int(data["SeriesEpisode"]),
                                         year=int(data["MovieYear"]))
        elif data["MovieKind"] ==  "movie" or data["MovieKind"]=="video" or data["MovieKind"]=="tv movie" \
                  or data["MovieKind"]=="tv series" :
            return self.factory.movie(imdbid="tt"+data["MovieImdbID"],
                                             year=int(data["MovieYear"]))
        else:
            raise TypeError("uknown video type: '%s'" % data["MovieKind"] )

