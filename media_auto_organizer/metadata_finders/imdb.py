__author__ = 'john'

from scrappers import imdbpie
from .metadata import Metadata
from datetime import datetime
from persistance import db_schema

class Imdb(Metadata):

    def update_metadata(self, file):
        self.get_imdb_metadata(file)

    def get_imdb_metadata(self, file):
        """
         Uses the  imdpie library to get info of a video and converts to the local datatypes
        """
        imdb = imdbpie.Imdb()

        media = file.media
        data = imdb.find_movie_by_id(media.imdbid)
        if not data:
            raise LookupError
        media.title = data.title
        media.tagline = data.tagline
        media.rating = data.rating
        media.year = data.year
        try:
            media.release_date = datetime.strptime(data.release_date, '%Y-%m-%d').date()
        except TypeError:
            pass

        media.plot_outline = data.plot_outline
        media.runtime = data.runtime
        media.cover_url = data.cover_url
        media.poster_url = data.poster_url

        for genre in data.genres:
            new_genre = self.factory.genre(name=genre)
            media.genres.append(new_genre)

        job = self.factory.job(description='Director')
        for director in data.directors_summary:
            new_person = self.factory.person(imdbid= director.imdb_id, name=director.name, job=job, media=media)

        job = self.factory.job(description='Writer')
        for writer in data.writers_summary:
            new_person = self.factory.person(imdbid= writer.imdb_id, name=writer.name, job=job, media=media)

        job = self.factory.job(description='Cast')
        for cast in data.cast_summary:
            new_person = self.factory.person(imdbid= cast.imdb_id, name=cast.name, job=job, media=media)

        if type(media) == db_schema.Episode:
            series_imdb = data.data["series"]["tconst"]
            imdbdata = imdb.find_movie_by_id(series_imdb)
            new_series = self.factory.series( imdbid= series_imdb, tagline=imdbdata.tagline, title=imdbdata.title,
                                       rating= imdbdata.rating, year=imdbdata.year,plot_outline=imdbdata.plot_outline,
                                       cover_url=imdbdata.cover_url,poster_url=imdbdata.poster_url )

            media.series = new_series