__author__ = 'john'

from abc import ABC, abstractmethod


class Metadata(ABC):
    """Abstract method for the metadata finders"""

    def __init__(self, object_factory):
        self.factory = object_factory

    @abstractmethod
    def update_metadata(self, file):
        """Inserts information in the metadata object of a file
        """
        pass

